//
//  SpinViewController.swift
//  Food Wheel
//
//  Created by Anthony Dimeo on 5/9/16.
//  Copyright © 2016 Full Sail University. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation
import MapKit


class SpinViewController: UIViewController, CLLocationManagerDelegate, UITextFieldDelegate {
    
    //Containing the Applications Settings Screen
    @IBOutlet weak var settingsView: UIView!
    
    //Instructions Labels
    @IBOutlet weak var howToShake: UILabel!
    @IBOutlet weak var touchHere: UILabel!
    @IBOutlet weak var spinsLabel: UILabel!
    
    
    //How many Spins Left
    @IBOutlet weak var spinsLeft: UILabel!
    //You can modify the Spin Counter so you can test all of   TableViews Coins ▼▼Here▼▼
    //    var spinCounter = 50
    var spinCounter = 3
    
    //This is the actual spinwheel
    @IBOutlet weak var spinWheel: UIImageView!
    
    //rotating boolean
    var rotating = false
    
    //Double for Lat and Long
    var lat: Double = 0.0
    var long: Double = 0.0
    
    
    
    
    
    
    //This unhides the Settings Screen
    @IBAction func settingsButtonAction(sender: UIButton) {
        settingsView.hidden = false
    }
    //This hides the Settings Screen
    @IBAction func backButtonAction(sender: UIButton) {
        settingsView.hidden = true
    }
    
    //Prepare For Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let resultVC = segue.destinationViewController as? ResultsViewController {
            searchAPIURL = resultVC.searchUrl
        }
        
    }
    //Unwind To Root
    //When we come back to the spin screen we subtract a spin
    @IBAction func unwindToRoot(segue: UIStoryboardSegue) {
        let purchasesVC = segue.sourceViewController as!
        AppPurchasesViewController
        
        purchasesVC.counterOfSpins = spinCounter
        spinsLeft.text = "\(spinCounter) Spins Left"
    }
    
    
    
    
    
    
    
    //Timer and seconds on spin timer
    var timer = NSTimer()
    var seconds = 5
    func setupTimer()  {
        seconds = 5
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("SubtractTime"), userInfo: nil, repeats: true)
        print(timer)
    }
    
    //Function to subtract the time
    func SubtractTime() {
        seconds--
        //If spin seconds have dropped to zero execute this code
        if(seconds == 0)  {
            //If the spin counter has dropped to zero execute this code
            if spinCounter == 0 {
                //To negate the subtracting spins
                self.spinCounter++
                
                //Pop up an alert so the user can't get to the next screen without spins
                let alert = UIAlertController(title: "Uh-Oh", message: "Your out of spins! You get 3 spins per day so you can wait until tomorrow or buy more now!", preferredStyle: UIAlertControllerStyle.Alert)
                
                alert.addAction(UIAlertAction(title: "Ill wait", style: UIAlertActionStyle.Cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Buy More", style: UIAlertActionStyle.Default, handler: { action in
                    switch action.style{
                        //So we can go directly to the app purchases screen
                    case .Default:
                        print("default")
                        let purchasesVC =  self.storyboard?.instantiateViewControllerWithIdentifier("AppPurchase") as! AppPurchasesViewController
                        self.presentViewController(purchasesVC, animated: true, completion: {
                            
                        })
                        
                    default:
                        print("shouldnt be hit")
                        
                    }
                }))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            //Stops rotating the wheel, invalidates the timer, subtracts spins from the counter and prompts the next screen
            stopRotating()
            timer.invalidate()
            self.spinCounter--
            self.spinsLeft.text = "\(self.spinCounter) Spins Left"
            
            let resultsVC =  self.storyboard?.instantiateViewControllerWithIdentifier("ResultsPage") as! ResultsViewController
            self.presentViewController(resultsVC, animated: true, completion: {
                
            })
        }
    }
    
    
    
    
    
    
    
    
    
    
    //Our Motion manager for shaking the device
    var manager = CMMotionManager()
    //Our location manager to get our current location
    var locationManager:CLLocationManager!
    
    //If our location was authorized then start updating location
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedAlways {
            if CLLocationManager.isMonitoringAvailableForClass(CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    locationManager.startUpdatingLocation()
                    
                }
            }
            //if the location wasnt authorized stop updating location
        } else if status == .Denied {
            let alert = UIAlertController(title: "Error", message: "You need to allow locations to use this app: Go to your device settings and turn on location for this app", preferredStyle: UIAlertControllerStyle.Alert)
            self.presentViewController(alert, animated: true, completion: nil)
            locationManager.stopUpdatingLocation()
        }
    }
    
    //Did update locations and then we grab our zip code
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let allowedStatus = CLLocationManager.authorizationStatus()
        
        if allowedStatus == CLAuthorizationStatus.AuthorizedWhenInUse {
            locationManager!.requestWhenInUseAuthorization()
            
            
            let coord : CLLocationCoordinate2D = manager.location!.coordinate
            lat = coord.latitude
            long = coord.longitude
            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    //
    //    override func motionEnded(motion: UIEventSubtype, withEvent event: UIEvent?) {
    //        if motion == .MotionShake {
    //            print("Shaked")
    //          testLabel.layer.backgroundColor = UIColor.blueColor().CGColor
    //            UIView.animateWithDuration(1.0, delay: 0.0, options: .CurveEaseIn
    //                , animations: { () -> Void in
    //                    self.spinWheel.transform = CGAffineTransformRotate(self.spinWheel.transform, CGFloat(M_PI_2))
    //                }, completion: nil)
    //
    //
    //
    //        }
    //    }
    //
    
    //Our function to rotate the view
    private func rotateView(targetView: UIView, duration: Double = 1.0) {
        UIView.animateWithDuration(duration, delay: 0.0, options: .CurveLinear, animations: {
            targetView.transform = CGAffineTransformRotate(targetView.transform, CGFloat(M_PI))
            }) { finished in
                self.rotateView(targetView, duration: duration)
        }
    }
    
    
    
    //This function will check if the app has been launched once or not already
    let defaults = NSUserDefaults.standardUserDefaults()
    //we return a bool so we can return if its true or not that the app has launched already
    func tutorialLaunched() -> Bool {
        
        //If the app has already launched once
        if let tutorialHasLaunched = defaults.stringForKey("tutorialLaunched") {
            print(tutorialHasLaunched)
            howToShake.hidden = true
            touchHere.hidden = true
            spinsLabel.hidden = true
            return true
        } else {
            //Otherwise this must be the first time
            defaults.setBool(true, forKey: "tutorialLaunched")
            howToShake.hidden = false
            touchHere.hidden = false
            spinsLabel.hidden = false
            
            return false
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //Our function to start rotating
    func startRotating() {
        rotating = true
        rotate()
    }
    //Function to rotate
    func rotate() {
        UIView.animateWithDuration(1.0,
            delay: 0.0,
            options: .CurveLinear,
            animations: {self.spinWheel.transform = CGAffineTransformRotate(self.spinWheel.transform, 3.14)},
            completion: {finished in self.rotateAgain()})
    }
    //Function to rotate it again to simulate an neverending spin
    func rotateAgain() {
        UIView.animateWithDuration(1.0,
            delay: 0.0,
            options: .CurveLinear,
            animations: {self.spinWheel.transform = CGAffineTransformRotate(self.spinWheel.transform, 3.14)},
            completion: {finished in
                if self.rotating {
                    self.rotate()
                }
        })
    }
    //Our function to stop rotating
    func stopRotating() {
        rotating = false
    }
    
    
    
    
    
    //Search Api URL
    var searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&near=Orlando&v=20140806&m=foursquare"
    //different tiers of prices
    var tier1 = 1
    var tier2 = 2
    var tier3 = 3
    var tier4 = 4
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Set seconds to five
        //request location
        seconds = 5
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        settingsView.hidden = true
        
        //This is where we call the function to see if our app has already been launched
        tutorialLaunched()
        
        //Function for which state the switches are at
        switchStateChanged(UISwitch.init())
        
        
        //If the devices motion exceeds that of gravity + whatever the device exerts in force
        //We will use 2.5 newtons of force for a hard enough shake where it wont detect on its own
        if manager.deviceMotionAvailable {
            manager.deviceMotionUpdateInterval = 0.02
            manager.startDeviceMotionUpdatesToQueue(NSOperationQueue.mainQueue(), withHandler: {
                (handler, error) -> Void in
                if handler!.userAcceleration.x < -2.5 {
                    print("Shaked")
                    
                    self.howToShake.hidden = true
                    self.touchHere.hidden = true
                    self.spinsLabel.hidden = true
                    
                    //Once shaken we will rotate the wheel and start the countdown timer
                    self.startRotating()
                    self.setupTimer()
                }
            })
            
        }
    }
    
    
    
    
    
    //Search distance label and slider
    @IBOutlet weak var searchDistanceLabel: UILabel!
    @IBOutlet weak var searchDistanceSlider: UISlider!
    @IBAction func sliderEventChanged(sender: UISlider) {
        //This updates the slider to the integer of where it currently is
        
        let currentValue = Int(sender.value)
        searchDistanceLabel.text = "\(currentValue) mi"
        searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(currentValue)&section=restaurant&near=Orlando&v=20140806&m=foursquare"
        
    }
    
    //These are all our switches
    @IBOutlet weak var fastFoodSwitch: UISwitch!
    @IBOutlet weak var casualDiningSwitch: UISwitch!
    @IBOutlet weak var restaurantSwitch: UISwitch!
    
    @IBOutlet weak var cheapSwitch: UISwitch!
    @IBOutlet weak var pricySwitch: UISwitch!
    @IBOutlet weak var expensiveSwitch: UISwitch!
    
    
    
    
    
    
    //This function changes the state of our switches
    //Depending on what is currently on will change our search information
    @IBAction func switchStateChanged(sender: UISwitch) {
        
        
        
        if fastFoodSwitch.on {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&section=fast_food&near=Orlando&v=20140806&m=foursquare"
            casualDiningSwitch.on = false
            restaurantSwitch.on = false
        } else {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&section=food&near=Orlando&v=20140806&m=foursquare"
        }
        if casualDiningSwitch.on {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&section=dine_in&near=Orlando&v=20140806&m=foursquare"
            fastFoodSwitch.on = false
            restaurantSwitch.on = false
        } else {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&section=food&near=Orlando&v=20140806&m=foursquare"
        }
        if restaurantSwitch.on {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&query=restaurant&near=Orlando&v=20140806&m=foursquare"
            fastFoodSwitch.on = false
            casualDiningSwitch.on = false
        } else {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&section=food&near=Orlando&v=20140806&m=foursquare"
        }
        
        
        
        
        
        
        if cheapSwitch.on && pricySwitch.on == false && expensiveSwitch.on == false {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&price=1&section=restaurant&near=Orlando&v=20140806&m=foursquare"
        } else if pricySwitch.on && cheapSwitch.on == false && expensiveSwitch.on == false {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&price=2&section=restaurant&near=Orlando&v=20140806&m=foursquare"
            
        } else if expensiveSwitch.on && pricySwitch.on == false && cheapSwitch.on == false {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&price=3&section=restaurant&near=Orlando&v=20140806&m=foursquare"
        } else if cheapSwitch.on && pricySwitch.on && expensiveSwitch.on == false {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&price=1,2&section=restaurant&near=Orlando&v=20140806&m=foursquare"
        } else if cheapSwitch.on && pricySwitch.on && expensiveSwitch.on {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&price=1,2,3&section=restaurant&near=Orlando&v=20140806&m=foursquare"
        } else if cheapSwitch.on && cheapSwitch.on == false && expensiveSwitch.on {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA 33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&price=1,3&section=restaurant&near=Orlando&v=20140806&m=foursquare"
        } else if cheapSwitch.on == false && pricySwitch.on && expensiveSwitch.on {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&price=2,3&section=restaurant&near=Orlando&v=20140806&m=foursquare"
        } else if cheapSwitch.on == false && pricySwitch.on == false && expensiveSwitch.on == false {
            searchAPIURL = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&radius=\(Int(sliderEventChanged(searchDistanceSlider)))&section=restaurant&near=Orlando&v=20140806&m=foursquare"
        }
        
        
    }
    
}

























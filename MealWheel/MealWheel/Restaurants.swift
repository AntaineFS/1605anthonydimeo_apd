//
//  Restaurants.swift
//  MealWheel
//
//  Created by Anthony Dimeo on 5/28/16.
//  Copyright © 2016 Full Sail University. All rights reserved.
//

import Foundation
import UIKit

//custom class
class Restaurants {
    var restaurantString: String
    var dealString: String
    var webURL: String
    var coinsNeeded: Int
    
    //intializer
    init(name: String, deal: String, url: String, coin: Int) {
        self.restaurantString = name
        self.dealString = deal
        self.webURL = url
        self.coinsNeeded = coin
    }
}
//
//  AppPurchasesViewController.swift
//  MealWheel
//
//  Created by Anthony Dimeo on 5/21/16.
//  Copyright © 2016 Full Sail University. All rights reserved.
//

import UIKit

class AppPurchasesViewController: UIViewController {
    //This counts the spins that we have left
    var counterOfSpins = 0
    
    var six = 6
    var twelve = 12
    var twentyfour = 24
    
    //Not Needed action but we still need a name for it
    //Reason why is we have an unwind to root on the spin screen which will act as our back button to send data backwards
    @IBAction func backButton(sender: UIButton) {
        
    }
    
    
    //6 more spins
    @IBAction func sixMoreSpins(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")!)
        counterOfSpins = six
        
    }
    //12 more spins
    @IBAction func twelveMorespins(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")!)
        counterOfSpins = twelve
    }
    //24 more spins
    @IBAction func twentyFourMourSpins(sender: UIButton) {
        UIApplication.sharedApplication().openURL(NSURL(string: "itms://itunes.apple.com/de/app/x-gift/id839686104?mt=8&uo=4")!)
        counterOfSpins = twentyfour
        
    }
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sixMoreSpins(UIButton.init())
        twelveMorespins(UIButton.init())
        twentyFourMourSpins(UIButton.init())
        // Do any additional setup after loading the view.
    }
}

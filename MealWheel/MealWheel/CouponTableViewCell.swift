//
//  CouponTableViewCell.swift
//  MealWheel
//
//  Created by Anthony Dimeo on 5/28/16.
//  Copyright © 2016 Full Sail University. All rights reserved.
//

import UIKit

class CouponTableViewCell: UITableViewCell {
    
    //Cell outlets
    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var dealCell: UILabel!
    @IBOutlet weak var coinCell: UILabel!
    
    
    //function for setting the cells labels
    func setCell(labelText: String, dealText: String, coinText: Int) {
        self.nameCell.text = labelText
        self.dealCell.text = dealText
        self.coinCell.text = "\(coinText) coins"
    }
}

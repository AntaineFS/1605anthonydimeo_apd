//
//  GrouponViewController.swift
//  MealWheel
//
//  Created by Anthony Dimeo on 5/28/16.
//  Copyright © 2016 Full Sail University. All rights reserved.
//

import UIKit

class GrouponViewController: UIViewController {
    
    
    
    
    
    //Our empty string to hold the url
    var emptyString: String?
    //Webview outlet and variable
    var webView: UIWebView?
    @IBOutlet weak var myWebView: UIWebView!
    //base empty url
    var url = "https://www.groupon.com/"
    //This function optionally binds the empty string to the url on this screen
    func websiteFunc() {
        if let someLabel = emptyString {
            url = someLabel
        }
        
        
        
        
        
        //Web url based on what was selected in the tableview
        if let webUrl = NSURL(string: url) {
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: config)
            let request = NSURLRequest(URL: webUrl)
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true;
            
            session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                guard error == nil else {
                    print("error occured \(error)")
                    return;
                }
                //200 Status code then proceed to load the webview
                if let httpResponse = response as? NSHTTPURLResponse where httpResponse.statusCode == 200 {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.myWebView.loadRequest(request)
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true;
                        
                        
                    })
                }
            }).resume()
        }
    }
    
    
    
    //Dismiss this and go back to the Results Screen
    @IBAction func backButton(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Inititialize the webview and call our optionally binding function
        self.webView = UIWebView()
        websiteFunc()
        
    }
    
    
    
    
    
    
}

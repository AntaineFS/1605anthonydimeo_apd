



//
//  ResultsViewController.swift
//  MealWheel
//
//  Created by Anthony Dimeo on 5/21/16.
//  Copyright © 2016 Full Sail University. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import WebKit
import GameplayKit


//Cell reuse identifier
var reuseIdentifier = "Cell"
class ResultsViewController: UIViewController, CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    //location string empty for now
    var location: String = ""
    
    //Our lat and long variable
    var lat: Double = 0.0
    var long: Double = 0.0
    //counts our coins we have
    var coinCounter = 0
    @IBOutlet weak var coinsLabel: UILabel!
    
    //Location Manager
    var locationManager:CLLocationManager!
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedAlways {
            if CLLocationManager.isMonitoringAvailableForClass(CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    locationManager.startUpdatingLocation()
                }
            }
        }
    }
    
    //Search URL
    var searchUrl = "https://api.foursquare.com/v2/venues/explore?client_id=JE1FC5XGOAVFKTSMF3IXLVPXLY2A5DCDFUQM3NWR0FWV2LA5&client_secret=MI2L3R55N0B0W3F5NTTS30VTKNWHIG55VUDA33QNYYVZG5TM&query=restaurant&v=20140806&m=foursquare"
    
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let allowedStatus = CLLocationManager.authorizationStatus()
        //These will get our coordinates in the form of a latitude and longitude
        let coord : CLLocationCoordinate2D = locationManager.location!.coordinate
        lat = coord.latitude
        long = coord.longitude
        //Calling our API function
        getRestaurantData()
        
        print("\(coord.latitude)")
        print("\(coord.longitude)")
        
        if allowedStatus == CLAuthorizationStatus.AuthorizedWhenInUse {
            locationManager!.requestWhenInUseAuthorization()
            
            
        }
    }
    
    //Function to update the coins
    func CoinCounter() {
        coinCounter++
        coinsLabel.text = String(coinCounter)
    }
    
    //To spin again we dismiss the view controller
    @IBAction func spinAgainButton(sender: UIButton) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    //This is the label where we display the chosen restaurant
    @IBOutlet weak var resultingFoodPlace: UILabel!
    
    //Function we use to download the restaurant from the API
    func getRestaurantData() {
        //storing lat and long in a variable
        location = "&ll=\(lat),\(long)"
        
        
        
        
        
        //ADD lat and long to our searchAPI url
        if let url = NSURL(string: "\(searchUrl)\(location)"){
            
            //Setting up for our download
            let config = NSURLSessionConfiguration.defaultSessionConfiguration()
            let session = NSURLSession(configuration: config)
            let request = NSURLRequest(URL: url)
            //Showing a network Activity Indicator for slow loading
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true;
            session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
                guard error == nil else {
                    print("error occured \(error)")
                    return;
                }
                if let httpResponse = response as? NSHTTPURLResponse where httpResponse.statusCode == 200, let data = data {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = true;
                        //do try our SwiftyJsonParse Function
                        //Catch an error if there is one
                        do {
                            try self.SwiftyJsonParse(data)
                            
                        } catch let error as NSError {
                            print("some error \(error)");
                            return;
                            
                        }
                    })
                }
            }).resume()
        }
    }
    
    //Our function using the custom Swifty Json Parse method
    private func SwiftyJsonParse(data: NSData) throws {
        var error: NSError?
        let json = JSON(data: data, options: .MutableContainers, error: &error)
        print("THIS IS HITT! and also pulling json data")
        guard error == nil else {
            print ("error occured \(error)")
            return;
        }
        
        //Our first Object
        if let dataFromFS = json["response"].dictionary {
            //Then dive into the array
            if let data2 = dataFromFS["groups"]!.array {
                //we then loop through the array
                for child in data2 {
                    //The second array
                    if let name = child["items"].array {
                        //Loop through the second array
                        for childs in name {
                            //The venues parameter contains the name of the restaurant
                            if let name2 = childs["venue"]["name"].string {
                                //Update the label
                                resultingFoodPlace.text = name2
                                print(resultingFoodPlace.text)
                                
                                
                            }
                            
                        }
                    }
                    
                }
                
            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    //For the table View information
    @IBOutlet weak var myTableView: UITableView!
    //Our array containing the class
    var restaurantArray: [Restaurants] = [Restaurants]()
    
    //restaurant function
    //We will have 14 restaurants for our current deals
    func RestaurantFunc() {
        let name1 = Restaurants(name: "Mellow Mushroom", deal: "40% off: Two or more", url: "https://www.groupon.com/deals/mellow-mushroom-sanford-fl", coin: 10)
        let name2 = Restaurants(name: "The porch", deal: "40 % off", url: "https://www.groupon.com/deals/the-porch", coin: 10)
        let name3 = Restaurants(name: "The Wine Barn", deal: "43% Off: Wine and cuisine", url: "https://www.groupon.com/deals/the-wine-barn-no-gtg", coin: 15)
        let name4 = Restaurants(name: "Gregs Place", deal: "40% Off Pub food", url: "https://www.groupon.com/deals/greg-s-place", coin: 15)
        let name5 = Restaurants(name: "Kirin Sushi", deal: "40% Off", url: "https://www.groupon.com/deals/kirin-sushi-1", coin: 20)
        let name6 = Restaurants(name: "Sumo Japanese Steak House", deal: "$15 for $30", url: "https://www.groupon.com/deals/sumo-winter-park", coin: 20)
        let name7 = Restaurants(name: "Burger Craft", deal: "45% off", url: "https://www.groupon.com/deals/burger-craft-1", coin: 20)
        let name8 = Restaurants(name: "Pisces Rising", deal: "47% Off", url: "https://www.groupon.com/deals/pisces-rising", coin: 25)
        let name9 = Restaurants(name: "UNO Pizzeria & Grill ", deal: "$82 for Four $25 Gift Cards", url: "https://www.groupon.com/deals/uno-restaurants-llc-23", coin: 35)
        let name10 = Restaurants(name: "Anthonys Pizza", deal: "50% Off", url: "https://www.groupon.com/deals/anthony-s-pizza-orange-ave", coin: 35)
        let name11 = Restaurants(name: "Vinzos Italian Grill and Pizza", deal: "$35: Dinner for Two", url: "https://www.groupon.com/deals/vinzo-s-italian-grill-pizzeria", coin: 40)
        let name12 = Restaurants(name: "Hokkaido Japan Sushi & Teppan", deal: "48% Off", url: "https://www.groupon.com/deals/hokkaido-japanese-teppan-sushi", coin: 40)
        let name13 = Restaurants(name: "Royal Indian Cuisine", deal: "53% Off", url: "https://www.groupon.com/deals/royal-indian-cuisine-1", coin: 45)
        let name14 = Restaurants(name: "Wing Zone", deal: "50% off ", url: "https://www.groupon.com/deals/wing-zone-orlando", coin: 50)
        
        
        
        //Appends the restaurants to the array
        restaurantArray.append(name1)
        restaurantArray.append(name2)
        restaurantArray.append(name3)
        restaurantArray.append(name4)
        restaurantArray.append(name5)
        restaurantArray.append(name6)
        restaurantArray.append(name7)
        restaurantArray.append(name8)
        restaurantArray.append(name9)
        restaurantArray.append(name10)
        restaurantArray.append(name11)
        restaurantArray.append(name12)
        restaurantArray.append(name13)
        restaurantArray.append(name14)
        
        
        
    }
    
    
    
    //Just one row in the tableview section
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantArray.count;
    }
    
    //for our chosen cells
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //Dequeue cells not used
        let cell: CouponTableViewCell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath) as! CouponTableViewCell
        //Index Path
        let show = restaurantArray[indexPath.row]
        
        //Set the custom cells image and name
        cell.setCell(show.restaurantString, dealText: show.dealString, coinText: (show.coinsNeeded))
        //Accessory Type
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        
        //Disclosure Indicator can't be used as a button to be segued to another page it is just indicating there is something else.
        return cell;
    }
    
    
    
    
    //Our prepare for segue to show the Groupon ad
    //This sends over the url of the chosen restaurant in the class and populates the webview on the next screen
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let secondVC = segue.destinationViewController as? GrouponViewController, cell = sender as? CouponTableViewCell,
            //Index path for cell selected
            let indexPath = myTableView.indexPathForCell(cell) {
                
                //Pass over a reference
                let show = restaurantArray[indexPath.row]
                secondVC.emptyString = show.webURL
        }
        
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableview delegate and datasource
        myTableView.delegate = self
        myTableView.dataSource = self
        //Calling our restaurant tableview function
        RestaurantFunc()
        //Location information
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        //Coin counter
        coinCounter++
        coinsLabel.text = String(coinCounter)
        
        
        
        
    }
}

//
//  ViewController.swift
//  Food Wheel
//
//  Created by Anthony Dimeo on 5/2/16.
//  Copyright © 2016 Full Sail University. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    //Location manager variable
    var locationManager = CLLocationManager();
    
    
    
    func locationManager(manager: CLLocationManager,
        didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status == .AuthorizedAlways || status == .AuthorizedWhenInUse {
            manager.startUpdatingLocation()
         presentViewController(<#T##viewControllerToPresent: UIViewController##UIViewController#>, animated: <#T##Bool#>, completion: <#T##(() -> Void)?##(() -> Void)?##() -> Void#>)
        }
    }
   
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let allowedStatus = CLLocationManager.authorizationStatus()
        
        if allowedStatus == CLAuthorizationStatus.NotDetermined {
            locationManager.requestWhenInUseAuthorization()
            
        }
    }
//        } else if allowedStatus == CLAuthorizationStatus.AuthorizedWhenInUse {
//            locationManager.requestWhenInUseAuthorization()
//            locationManager.startUpdatingLocation()
//        }
//    }
   
    
    
//    func locationManager(manager: CLLocationManager,
//        didChangeAuthorizationStatus status: CLAuthorizationStatus)
//    {
//        switch CLLocationManager.authorizationStatus() {
//        case .AuthorizedAlways: break
//            // ...
//        case .NotDetermined:
//            manager.requestAlwaysAuthorization()
//        case .AuthorizedWhenInUse,
//        .Restricted,
//        .Denied:
//            
//            let alertController = UIAlertController(
//                title: "Location Disabled",
//                message: "Enter a zipcode so we know where you are.",
//                preferredStyle: .Alert)
//            
//            let cancelAction = UIAlertAction(title: "Okay", style: .Cancel, handler: nil)
//            alertController.addAction(cancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//            
//            
//        }
//
//        }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            locationManager.delegate = self;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        }
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
//        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

